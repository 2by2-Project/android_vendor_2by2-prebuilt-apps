#
#  Copyright (C) 2024 The 2by2 Project
#
#  SPDX-License-Identifier: Apache-2.0
#

LOCAL_PATH := vendor/2by2-prebuilt-apps

PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

ifneq ($(TARGET_USES_MINI_GAPPS), true)

PRODUCT_PACKAGES += \
    LINE \
    Niconico \
    PayPay

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/default-permissions-2by2-extra-apps.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/default-permissions/default-permissions-2by2-extra-apps.xml

endif
